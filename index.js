let newArr = [];
let howMany = 0, temp = 0, turns = 0, cross = 0, circle = 0;
let tile = document.getElementsByClassName('tile');
let cr = document.getElementsByClassName('cross');
let ci = document.getElementsByClassName('circle');

for (let i = 0; i < 3; i++) {
  newArr[i] = [];
  for (let j = 0; j < 3; j++) {
    howMany++
    newArr[i][j] = howMany
    temp = document.createElement('div');
    temp.className = 'tile';
    temp.innerHTML = howMany;
    document.querySelector('.box').appendChild(temp).textContent = '';
  }
}

Element.prototype.remove = function() {
  this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
  for(var i = this.length - 1; i >= 0; i--) {
      if(this[i] && this[i].parentElement) {
          this[i].parentElement.removeChild(this[i]);
      }
  }
}

isEnded = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[0][i] === arr[1][i] && arr[1][i] === arr[2][i] && arr[0][i] === arr[2][i]) return true
    if (arr[i][0] === arr[i][1] && arr[i][1] === arr[i][2] && arr[i][0] === arr[i][2]) return true
  } return false
}

isDiagonal = (arr) => {
  let x= 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][arr.length - i - 1] !== 'x' && arr[i][arr.length - i - 1] !== 'o') break;
    x += arr[i][arr.length - i - 1] === 'x' ? 1 : -1
  }
  return Math.abs(x) === 3
}

isDiagonal1 = (arr) => {
  let x= 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][i] !== 'x' && arr[i][arr.length - i - 1] !== 'o') break;
    x += arr[i][i] === 'x' ? 1 : -1
  }
  return Math.abs(x) === 3
}

newArr.map((row, r) => (
  row.map((cell, c) => {
    tile[cell - 1].addEventListener('click', (id) => {
      if (this.isDiagonal1(newArr) || this.isDiagonal(newArr) || this.isEnded(newArr)) {
        return;
      }

      if (turns % 2 === 0 && newArr[r][c] !== 'o' && newArr[r][c] !== 'x') {
        newArr[r][c] = 'x'
        cross = document.createElement('div');
        cross.className = 'cross'
        tile[cell - 1].appendChild(cross)
        turns++
      }

      if (turns % 2 !== 0 && newArr[r][c] !== 'x' && newArr[r][c] !== 'o') {
        newArr[r][c] = 'o'
        circle = document.createElement('div');
        circle.className = 'circle'
        tile[cell - 1] = tile[cell - 1].appendChild(circle)
        turns++
      }
    })
  })
))



document.querySelector('.btn-die').addEventListener('click',   function(){
  howMany=0;
  for (let i = 0; i < 3; i++) {
    newArr[i] = [];
    for (let j = 0; j < 3; j++) {
      howMany++
      newArr[i][j] = howMany
      cr.remove();
      ci.remove();
    }
  }
});